//
//  CartVC.swift
//  Cart
//
//  Created by Alejandro Jiménez Agudo on 20/11/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import UIKit


class CartVC: UIViewController, UITableViewDataSource, CartView {
	
	private var products: [Product]?
	private let presenter = CartPresenter()
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var labelTotalAmount: UILabel!
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.presenter.view = self
		self.presenter.viewDidLoad()
	}
	
	
	// MARK: - Presenter
	
	func showProducts(products: [Product]) {
		self.products = products
		self.tableView.reloadData()
	}
	
	func showTotalAmount(amount: Double) {
		self.labelTotalAmount.text = "\(amount)€"
	}
	
	// MARK: - TableView DataSource
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let number = self.products?.count {
			return number
		}
		else {
			return 0
		}
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("CellProduct") as! ProductCell
		
		let product = self.products![indexPath.row]
		let quantity = self.presenter.shouldShowQuantity(product)
		
		cell.productName.text = product.productName + " (x\(quantity))"
		cell.productPrice.text = product.productPrice + "€"
	
		return cell
	}

}
